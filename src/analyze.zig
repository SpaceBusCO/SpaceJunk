const std = @import("std");
const utils = @import("utils.zig");
// Implementation of the "strings" command.
// TODO: FIXME
pub fn strings(file: *utils.binFile, print: bool, threshold: f16) !void {
    _ = print;
    _ = threshold;
    const stdout = std.io.getStdOut().writer();

    var start: ?usize = null;
    //if (file.entropy.items.len == 0) {
    //    try FileEntropy(file);
    //}
    for (file.buf, 0..) |val, i| {
        if (val <= 127 and val > 0) {
            if (start == null) {
                start = i;
            }
        } else if (start != null and i - start.? >= 3) {
            const localEntropy = entropy(file.buf[start.?..i]);
            if (localEntropy <= 0.55 and localEntropy >= 0.1) {
                try stdout.print("{s}\n", .{file.buf[start.?..i]});
                start = null;
            }
        } else {
            start = null;
        }
    }
    if (start != null) {
        std.debug.print("{s}\n", .{file.buf[start.?..file.size]});
    }
}
pub fn entropy(string: []const u8) f16 {
    var sum: u64 = 0;
    // The entropy function is as follows:
    // 𝛴 p(x) * log2(p(x))
    // Where p(x) is the probability of the occurance of x.
    // In other words, we are averages the number of 1's
    // occuring in a 16 byte segment of a file:
    // p(x) =  𝛴 (@popCount(val))/(16*8).
    for (string, 0..) |val, j| {
        _ = j;
        sum += @popCount(val);
    }
    if (sum == 0) {
        return 0;
    }
    const p: f16 = @as(f16, @floatFromInt(sum)) / (@as(f16, @floatFromInt(string.len * 8)));
    // TODO: implement @abs atomic once implemented upstream.
    return (-1 * (p * @log2(p)));
}
