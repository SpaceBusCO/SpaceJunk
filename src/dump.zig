const std = @import("std");
const utils = @import("utils.zig");
const analyze = @import("analyze.zig");
const fmt = std.fmt;
const stderr = std.debug.print;

const black = "\x1b[30m";
const red = "\x1b[31m";
const green = "\x1b[32m";
const yellow = "\x1b[33m";
const blue = "\x1b[34m";
const magenta = "\x1b[35m";
const cyan = "\x1b[36m";
const white = "\x1b[37m";
const dft = "\x1b[39m";
const rst = "\x1b[1;0m";

pub fn hexdump(file: *utils.binFile) !void {
    const stdout = std.io.getStdOut().writer();

    var i: usize = 0;
    var hexcode_length: u64 = 0;
    if (file.size > 0) {
        hexcode_length = @as(u64, @intFromFloat(@floor(@log(@as(f32, @floatFromInt(file.size))) / (2.772588722)))) + 1;
    }
    const length: usize = file.size + (16 - file.size % 16);

    try stdout.print(" {s}┏", .{red});
    for (0..hexcode_length + 77) |j| {
        if (j == hexcode_length + 5) {
            try stdout.print("┯", .{});
        } else if (j == 31 + hexcode_length) {
            try stdout.print("┯", .{});
        } else if (j == 57 + hexcode_length) {
            try stdout.print("┯", .{});
        } else if (j == 67 + hexcode_length) {
            try stdout.print("┯", .{});
        } else {
            try stdout.print("━", .{});
        }
    }

    try stdout.print("┓\n", .{});
    //TODO: Add in color coding to calculated entropy of file.
    while (i <= length) {
        if (i % 16 == 0) {
            if (i > 0) {
                try stdout.print(" {s}│{s} ", .{ red, rst });
                for (i - 16..i) |j| {
                    if (i - j == 8) {
                        try stdout.print("{s}┊", .{red});
                    }
                    var val: u8 = " "[0];
                    if (j < file.size) {
                        val = file.buf[j];
                    }
                    switch (val) {
                        // ascii printable characters.
                        32...126 => {
                            try stdout.print("{s}{c}", .{ green, val });
                        },
                        else => {
                            try stdout.print("{s}⋄", .{dft});
                        },
                    }
                }
                try stdout.print("{s} ┃\n", .{red});
            }
            if (i != length) {
                try stdout.print(" ┃ {s}0x", .{blue});

                // add lots of 0's to the thing.

                for (0..hexcode_length - @as(u64, @intFromFloat(@floor(@log(@as(f32, @floatFromInt(i + 1))) / (2.772588722))))) |j| {
                    _ = j;
                    try stdout.print("0", .{});
                }
                // Address.
                try stdout.print("{x} ", .{i});
                try stdout.print("{s}│", .{red});
                try stdout.print("{s}", .{rst});
            }
            // center divider
        } else if (i % 8 == 0) {
            try stdout.print(" {s}│", .{red});
            try stdout.print("{s}", .{rst});
        }
        // value.
        if (i < file.size) {
            var color = dft;
            switch (file.buf[i]) {
                32...126 => {
                    color = green;
                },
                else => {
                    color = dft;
                },
            }
            try stdout.print(" {s}{x:0>2}{s}", .{ color, file.buf[i], rst });
        } else if (i == length) {
            try stdout.print(" ", .{});
        } else {
            try stdout.print("   ", .{});
        }
        i += 1;
    }
    try stdout.print("{s}┗", .{red});
    for (0..hexcode_length + 77) |j| {
        if (j == hexcode_length + 5) {
            try stdout.print("┷", .{});
        } else if (j == 31 + hexcode_length) {
            try stdout.print("┷", .{});
        } else if (j == 57 + hexcode_length) {
            try stdout.print("┷", .{});
        } else if (j == 67 + hexcode_length) {
            try stdout.print("┷", .{});
        } else {
            try stdout.print("━", .{});
        }
    }

    try stdout.print("┛\n", .{});
}
