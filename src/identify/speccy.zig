const std = @import("std");
const utils = @import("../utils.zig");
const identify = @import("../identify.zig");

// Sinclair ZX Spectrum .SNA snapshot file format
// other formats to come
// TODO add a file_extensions field for formats without magic word signature

pub const file_format = identify.FileFormat{
    .id = .zx_sna,
    .name = "ZX Spectrum SNA",
    .verify = verify,
    .get_bits_endian = struct {
        fn get_bits_endian(file: *utils.binFile) ?[]const u8 {
            _ = file;
            return "8-bit, little-endian";
        }
    }.get_bits_endian,
    .get_arches = struct {
        fn get_arches(al: std.mem.Allocator, file: *utils.binFile) !?[]const []const u8 {
            _ = file;
            return try al.dupe([]const u8, &[_][]const u8{"z80"});
        }
    }.get_arches,
    .get_code_blocks = get_code_blocks,
    .get_entry_point = get_entry_point,
};

// there's no magic word signature
// but certain fields can only have specific values
// TODO some .sna files may be 49280 bytes due to +3DOS rounding up to nearest 128 bytes
// TODO https://groups.google.com/g/comp.sys.sinclair/c/MIOD3UQC_-c/m/CiV3sVXhJ2QJ

fn verify(file: *utils.binFile) bool {
    const sna48klen = 27 + 48 * 1024;
    const sna128klen = 27 + 3 + 128 * 1024;

    if (file.buf.len != sna48klen and file.buf.len != sna128klen) return false;

    // $13 IFF2 [Only bit 2 is defined: 1 for EI, 0 for DI]
    if ((file.buf[0x13] & 0b1111_1011) != 0) return false;

    // $19 Interrupt mode: 0, 1 or 2
    if (file.buf[0x19] > 2) return false;

    // $1a Border colour: 0-7
    // Instead of the border colour, the original Microdriver stores a copy of the
    // byte at address $0700, which is then used to detect whether the Interface 1
    // ROM is paged in or not — $71 for the Spectrum ROM, $C9 for the Interface 1 ROM
    if (file.buf[0x1a] > 7 and file.buf[0x1a] != 0x71 and file.buf[0x1a] != 0xc9) return false;

    return true;
}

fn get_code_blocks(al: std.mem.Allocator, file: *utils.binFile) anyerror!?[]const []const u8 {
    const ram = file.buf[0x1b..];

    // there's always one block and it's always the 48kb exactly starting at offset 0x1b in the file
    return try al.dupe([]const u8, &[_][]const u8{ram});
}

fn get_entry_point(file: *utils.binFile) ?identify.EntryPoint {
    // SP is at $17
    var sp = std.mem.readInt(u16, file.buf[0x17..][0..2], .little);

    const ram = file.buf[0x1b..];

    const ram_offset = 16384;

    var maybe_pc: ?u16 = null;
    if (sp >= ram_offset and sp < 0xffff) {
        // sp is in ram, pc is on stack
        maybe_pc = std.mem.readInt(u16, ram[sp - ram_offset ..][0..2], .little);
        sp +%= 2;
    }

    if (maybe_pc) |pc| {
        if (pc >= ram_offset) {
            // the pc is in ram, can use it as the entry point symbol
            var entry_point_offset: u17 = 0x1b;
            entry_point_offset += pc - ram_offset;

            return .{ .address = pc, .file_offset = entry_point_offset };
        } else {
            // the pc is in rom, can't use it as the entry point symbol
            return .{ .address = pc, .file_offset = null };
        }
    }
    // we don't know what the pc is
    return null;
}
