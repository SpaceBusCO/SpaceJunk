const std = @import("std");
const utils = @import("../utils.zig");
const identify = @import("../identify.zig");
const mem = std.mem;

// Mac OS resource forks and files
// https://en.wikipedia.org/wiki/Resource_fork
// Used by classic Mac OS
// Files had a data fork and a resource fork and some metadata embedded in the filesystem
// Applications have the 'APPL' type in the metadata and have 'CODE' resources
// 'CODE' 0 has a 'jump table' and all other 'CODE' resources have the machine code

pub const file_format = identify.FileFormat{
    .id = .mac_resource,
    .name = "Mac OS resource file",
    .verify = check,
    .get_bits_endian = struct {
        fn get_bits_endian(file: *utils.binFile) ?[]const u8 {
            _ = file;
            return "32-bit, big-endian";
        }
    }.get_bits_endian,
    .get_arches = struct {
        fn get_arches(al: std.mem.Allocator, file: *utils.binFile) !?[]const []const u8 {
            _ = file;
            return try al.dupe([]const u8, &[_][]const u8{"m68k"});
        }
    }.get_arches,
    .get_code_blocks = get_code_blocks,
};

fn check(file: *utils.binFile) bool {
    // resource forks do not use a magic word, but they do have a duplicate header
    // we can also do some sanity checks on offset and length fields
    // and there must be a CODE resource
    const header_len = @sizeOf(u32) * 4;
    if (header_len < file.size) {
        const data_offset = mem.readInt(u32, file.buf[0..4], .big);
        const map_offset = mem.readInt(u32, file.buf[4..][0..4], .big);
        const data_len = mem.readInt(u32, file.buf[8..][0..4], .big);
        const map_len = mem.readInt(u32, file.buf[12..][0..4], .big);

        // check if any these additions would overflow
        _ = std.math.add(u32, data_offset, data_len) catch return false;
        _ = std.math.add(u32, map_offset, map_len) catch return false;
        _ = std.math.add(u32, map_offset, header_len) catch return false;

        // header fields are within the bounds of the file
        if (file.size < data_offset + data_len or
            file.size < map_offset + map_len or
            file.size < map_offset + header_len) return false;

        // duplicate of header in the resource map matches
        if (mem.eql(u8, file.buf[map_offset..][0..header_len], file.buf[0..header_len])) {
            const resource_map = sl_stlen(file.buf, map_offset, map_len) orelse return false;

            const type_list_offset = std.mem.readInt(u16, sl_stlen(resource_map, header_len + 8, 2) orelse return false, .big);
            const name_list_offset = std.mem.readInt(u16, sl_stlen(resource_map, header_len + 10, 2) orelse return false, .big);

            const type_list = sl_stend(resource_map, type_list_offset, name_list_offset) orelse return false;

            const type_count: u16 = @intCast(std.mem.readInt(i16, sl_end(type_list, 2) orelse return false, .big) + 1);

            const type_entries = sl_stend(type_list, 2, type_count * 8) orelse return false;

            for (0..type_count) |i|
                if (mem.eql(u8, sl_stlen(type_entries, i * 8, 4) orelse return false, "CODE")) return true;
        }
    }
    return false;
}

fn get_code_blocks(al: std.mem.Allocator, file: *utils.binFile) anyerror!?[]const []const u8 {
    var slices = try std.ArrayList([]const u8).initCapacity(al, 10);
    defer slices.deinit();

    // check() already passed so we skip the checks in that code
    const header_len = @sizeOf(u32) * 4;
    const data_offset = mem.readInt(u32, file.buf[0..4], .big);
    const map_offset = mem.readInt(u32, file.buf[4..][0..4], .big);
    const data_len = mem.readInt(u32, file.buf[8..][0..4], .big);
    const map_len = mem.readInt(u32, file.buf[12..][0..4], .big);

    const resource_data = file.buf[data_offset..][0..data_len];
    const resource_map = file.buf[map_offset..][0..map_len];
    const typelist_offset = std.mem.readInt(u16, resource_map[header_len + 8 ..][0..2], .big);
    const namelist_offset = std.mem.readInt(u16, resource_map[header_len + 10 ..][0..2], .big);

    const resource_type_list = resource_map[typelist_offset..namelist_offset];
    const type_count: u16 = @intCast(std.mem.readInt(i16, resource_type_list[0..2], .big) + 1);
    const type_entries = resource_type_list[2..][0 .. type_count * 8];

    // check() did not cover the logic past here so we check all slice accesses
    for (0..type_count) |i| {
        const typelist_entry = sl_stlen(type_entries, i * 8, 8) orelse return null;
        const typ = sl_end(typelist_entry, 4) orelse return null;
        if (std.mem.eql(u8, typ, "CODE")) {
            const count = std.mem.readInt(u16, sl_stlen(typelist_entry, 4, 2) orelse return null, .big) + 1;
            const reslist_off = std.mem.readInt(u16, sl_stlen(typelist_entry, 6, 2) orelse return null, .big);
            const res_list = sl_stlen(resource_type_list, reslist_off, count * 12) orelse return null;

            for (0..count) |j| {
                const reslist_entry = sl_stlen(res_list, j * 12, 12) orelse return null;
                const id = std.mem.readInt(u16, sl_end(reslist_entry, 2) orelse return null, .big);

                if (id != 0) {
                    const offset_to_res_data = std.mem.readInt(u24, sl_stlen(reslist_entry, 5, 3) orelse return null, .big);
                    const resource_len = std.mem.readInt(u32, sl_stlen(resource_data, offset_to_res_data, 4) orelse return null, .big);

                    const offset_of_1st_jump_table_entry = mem.readInt(u16, sl_stlen(resource_data, offset_to_res_data + 4, 2) orelse return null, .big);

                    // near model only for now, add far model when I have a test file
                    if (offset_of_1st_jump_table_entry == 0xffff) unreachable;

                    // this does not include the 32-bit length field, which already omits itself
                    const header_size = 4; // for far model this is 40 bytes (0x28)
                    const footer_size = 0; // far model has 'A5 relocation info' and 'Segment relocation info'
                    const code_offset = data_offset + offset_to_res_data + 4 + header_size;
                    const code_len = resource_len - header_size - footer_size;

                    try slices.append(sl_stlen(file.buf, code_offset, code_len) orelse return null);
                }
            }
        }
    }

    if (slices.items.len == 0) {
        return null;
    } else {
        return try al.dupe([]const u8, slices.items);
    }
}

// wrapper for slice[start..end] that returns a null optional if out of bounds
inline fn sl_stend(slice: []const u8, start: usize, end: usize) ?@TypeOf(slice[start..end]) {
    if (slice.len < end) return null;
    return slice[start..end];
}

// wrapper for slice[start..][0..len] that returns a null optional if out of bounds
inline fn sl_stlen(slice: []const u8, start: usize, len: usize) ?@TypeOf(slice[start..][0..len]) {
    if (slice.len < start + len) return null;
    return slice[start..][0..len];
}

// wrapper for when you only have the end or the length, which turn out to be the same
inline fn sl_end(slice: []const u8, len: usize) ?@TypeOf(slice[0..len]) {
    if (slice.len < 0 + len) return null;
    return slice[0..len];
}
