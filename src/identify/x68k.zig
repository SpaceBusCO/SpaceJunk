const std = @import("std");
const utils = @import("../utils.zig");
const identify = @import("../identify.zig");
const mem = std.mem;

// Sharp X68000 .X files and .Z files
// https://datacrystal.romhacking.net/wiki/X68k/PM

pub const file_format_x = identify.FileFormat{
    .id = .x68k_x,
    .name = "Sharp X68000 .X file",
    .magic_words = &.{
        "HU\x00\x00", // normal
        "HU\x00\x01", // smallest block
        "HU\x00\x02", // high address
    },
    .get_bits_endian = struct {
        fn get_bits_endian(file: *utils.binFile) ?[]const u8 {
            _ = file;
            return "32-bit, big-endian";
        }
    }.get_bits_endian,
    .get_arches = struct {
        fn get_arches(al: std.mem.Allocator, file: *utils.binFile) !?[]const []const u8 {
            _ = file;
            return try al.dupe([]const u8, &[_][]const u8{"m68k"});
        }
    }.get_arches,
};

pub const file_format_z = identify.FileFormat{
    .id = .x68k_z,
    .name = "Sharp X68000 .Z file",
    .magic_words = &.{
        "\x60\x1a",
    },
    .verify = check_z,
    .get_bits_endian = struct {
        fn get_bits_endian(file: *utils.binFile) ?[]const u8 {
            _ = file;
            return "32-bit, big-endian";
        }
    }.get_bits_endian,
    .get_arches = struct {
        fn get_arches(al: std.mem.Allocator, file: *utils.binFile) !?[]const []const u8 {
            _ = file;
            return try al.dupe([]const u8, &[_][]const u8{"m68k"});
        }
    }.get_arches,
};

fn check_z(file: *utils.binFile) bool {
    return 0x1a + 2 <= file.buf.len and
        mem.eql(u8, file.buf[0x1a..][0..2], "\xff\xff");
}
