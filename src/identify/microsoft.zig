const std = @import("std");
const utils = @import("../utils.zig");
const identify = @import("../identify.zig");
const mem = std.mem;

// MS-DOS 'MZ' and derived executable formats
// https://en.wikipedia.org/wiki/DOS_MZ_executable
// For now, only PE (PE32 and PE32+) are supported
// https://en.wikipedia.org/wiki/Portable_Executable

pub const file_format_mz = identify.FileFormat{
    .id = .ms_dos,
    .name = "MS-DOS 'MZ' executable",
    .magic_words = &.{
        "MZ",
    },
    .get_bits_endian = struct {
        fn get_bits_endian(file: *utils.binFile) ?[]const u8 {
            _ = file;
            return "16-bit, little-endian";
        }
    }.get_bits_endian,
    .get_arches = struct {
        fn get_arches(al: std.mem.Allocator, file: *utils.binFile) !?[]const []const u8 {
            _ = file;
            return try al.dupe([]const u8, &[_][]const u8{"8086"});
        }
    }.get_arches,
};

pub const file_format_pe = identify.FileFormat{
    .id = .windows_pe,
    .name = "Windows 'PE' executable",
    .magic_words = &.{
        "MZ",
    },
    .verify = check_pe,
    .get_bits_endian = bit_end_pe,
    .get_arches = get_arches_pe,
};

fn check_pe(file: *utils.binFile) bool {
    // e_lfanew at offset 0x3c is the offset to the start of the PE header
    if (0x3c + 4 < file.size) {
        const pe_offset = mem.readInt(u32, file.buf[0x3c..][0..4], .little);
        if (pe_offset + 4 < file.size) {
            return mem.eql(u8, "PE\x00\x00", file.buf[pe_offset .. pe_offset + 4]);
        }
    }
    return false;
}

fn bit_end_pe(file: *utils.binFile) ?[]const u8 {
    const pe_offset = mem.readInt(u32, file.buf[0x3c..][0..4], .little);
    const pe_record = file.buf[pe_offset..];
    if (0x14 + 2 < pe_record.len) {
        const size_of_optional_header = mem.readInt(u16, pe_record[0x14..][0..2], .little);
        const optional_header = pe_record[0x18..][0..size_of_optional_header];
        if (2 < size_of_optional_header) {
            switch (mem.readInt(u16, optional_header[0..2], .little)) {
                0x10b => return "32-bit, little-endian",
                0x20b => return "64-bit, little-endian",
                else => return null,
            }
        }
    }
    return null;
}

fn get_arches_pe(al: std.mem.Allocator, file: *utils.binFile) anyerror!?[]const []const u8 {
    const pe_offset = mem.readInt(u32, file.buf[0x3c..][0..4], .little);
    const pe_record = file.buf[pe_offset..];
    var hw_arch: ?[]const u8 = null;
    if (4 + 2 < pe_record.len) {
        hw_arch = switch (mem.readInt(u16, pe_record[4..][0..2], .little)) {
            0x014c => "x86",
            0x8664 => "x64", // aka x86-64, amd64
            else => return null,
        };
    }
    if (0x14 + 2 < pe_record.len) {
        const size_of_optional_header = mem.readInt(u16, pe_record[0x14..][0..2], .little);
        const optional_header = pe_record[0x18..][0..size_of_optional_header];
        if (2 < size_of_optional_header) {
            const magic = mem.readInt(u16, optional_header[0..2], .little);
            const clr_runtime_header_offset: u32 = switch (magic) {
                0x10b => 0xd0,
                0x20b => 0xe0,
                else => unreachable,
            };
            if (clr_runtime_header_offset + 4 + 4 < size_of_optional_header) {
                const virtual_address = mem.readInt(u32, optional_header[clr_runtime_header_offset..][0..4], .little);
                const size = mem.readInt(u32, optional_header[clr_runtime_header_offset + 4 ..][0..4], .little);
                if (virtual_address != 0 or size != 0) {
                    return try al.dupe([]const u8, &[_][]const u8{ hw_arch.?, ".NET CLR" });
                }
            }
        }
    }
    return try al.dupe([]const u8, &[_][]const u8{hw_arch.?});
}
