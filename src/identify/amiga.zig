const std = @import("std");
const utils = @import("../utils.zig");
const identify = @import("../identify.zig");
const mem = std.mem;

// Amiga "Hunk" format
// https://en.wikipedia.org/wiki/Amiga_Hunk

pub const file_format = identify.FileFormat{
    .id = .amiga_hunk,
    .name = "Amiga Hunk",
    .magic_words = &.{
        "\x00\x00\x03\xf3", // loadable files
        "\x00\x00\x03\xe7", // object code and link libraries
    },
    .get_bits_endian = struct {
        fn get_bits_endian(file: *utils.binFile) ?[]const u8 {
            _ = file;
            return "32-bit, big-endian";
        }
    }.get_bits_endian,
    .get_arches = struct {
        fn get_arches(al: std.mem.Allocator, file: *utils.binFile) !?[]const []const u8 {
            _ = file;
            return try al.dupe([]const u8, &[_][]const u8{"m68k"});
        }
    }.get_arches,
};
