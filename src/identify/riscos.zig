const std = @import("std");
const utils = @import("../utils.zig");
const identify = @import("../identify.zig");
const mem = std.mem;

pub const file_format = identify.FileFormat{
    .id = .riscos,
    .name = "RISC OS AIF executable",
    .verify = check,
    .get_bits_endian = struct {
        fn get_bits_endian(file: *utils.binFile) ?[]const u8 {
            _ = file;
            return "32-bit, little-endian";
        }
    }.get_bits_endian,
    .get_arches = struct {
        fn get_arches(al: std.mem.Allocator, file: *utils.binFile) !?[]const []const u8 {
            _ = file;
            return try al.dupe([]const u8, &[_][]const u8{"arm"});
        }
    }.get_arches,
};

// header is 0x40 bytes long
// offset 0x10 contains 0x11 0x00 0x00 0xef, which is the assembly instruction swi OS_Exit - this is used by 'file'
// offset 0x24 'Image debug type' can only contain 0, 1, 2 or 3
// offset 0x30 contains either 0, 26, or 32 (0x00, 0x1a, 0x20)
// bit 31 of 0x39 indicates StrongARM. so: 0x80000000 set would add to: 0x8000001a or 0x80000020
fn check(file: *utils.binFile) bool {
    if (file.size < 0x40) return false;
    if (mem.readInt(u32, file.buf[0x10..][0..4], .little) != 0xef000011) return false;
    if (mem.readInt(u32, file.buf[0x24..][0..4], .little) > 3) return false;
    return switch (mem.readInt(u32, file.buf[0x30..][0..4], .little)) {
        0, 26, 32 => true,
        0x80000000, 0x80000000 | 26, 0x80000000 | 30 => true,
        else => false,
    };
}
