const std = @import("std");
const utils = @import("../utils.zig");
const identify = @import("../identify.zig");
const mem = std.mem;

// Mach-O
// https://en.wikipedia.org/wiki/Mach-O
// Used by modern macOS, iOS, tvOS, and watchOS

pub const file_format = identify.FileFormat{
    .id = .macho,
    .name = "Mach-O",
    .magic_words = &.{
        "\xFE\xED\xFA\xCE",
        "\xFE\xED\xFA\xCF",
        "\xCE\xFA\xED\xFE",
        "\xCF\xFA\xED\xFE",
    },
    .get_bits_endian = bit_end,
    .get_arches = get_arches,
};

pub const file_format_universal = identify.FileFormat{
    .id = .macho_universal,
    .name = "Mach-O Universal binary",
    .magic_words = &.{
        "\xCA\xFE\xBA\xBE",
        "\xCA\xFE\xBA\xBF",
        "\xBE\xBA\xFE\xCA",
        "\xBF\xBA\xFE\xCA",
    },
    .verify = check_universal,
    .get_bits_endian = bit_end_universal,
    .get_arches = get_arches_universal,
};

fn bit_end(file: *utils.binFile) ?[]const u8 {
    switch (mem.readInt(u32, file.buf[0..4], .big)) {
        0xFEEDFACE => return "32-bit, big-endian",
        0xCEFAEDFE => return "32-bit, little-endian",
        0xFEEDFACF => return "64-bit, big-endian",
        0xCFFAEDFE => return "64-bit, little-endian",
        else => return null,
    }
    return null;
}

fn get_arches(al: std.mem.Allocator, file: *utils.binFile) !?[]const []const u8 {
    const endian = switch (mem.readInt(u32, file.buf[0..4], .big)) {
        0xFEEDFACE, 0xFEEDFACF => std.builtin.Endian.big,
        0xCEFAEDFE, 0xCFFAEDFE => std.builtin.Endian.little,
        else => return null,
    };
    const cpu_type = mem.readInt(u32, file.buf[4..8], endian) & ~@as(u32, 0x1000000);
    const cpu_subtype = mem.readInt(u32, file.buf[8..12], endian);
    const arch = switch (cpu_type) {
        0x00000007 => switch (cpu_subtype) {
            else => "x86",
        },
        0x0000000C => switch (cpu_subtype) {
            else => "arm",
        },
        0x00000012 => switch (cpu_subtype) {
            0x00000009 => "ppc 750",
            0x0000000A => "ppc 7400",
            0x00000064 => "ppc 970",
            else => "ppc",
        },
        else => return null,
    };
    return try al.dupe([]const u8, &[_][]const u8{arch});
}

fn check_universal(file: *utils.binFile) bool {
    // java class files use the same magic word as Apple's universal binaries
    // java class files use the four bytes at offset 0x04 for the JVM minor and major version
    // minor is always 0, major currently ranges from 0x2D (JDK 1.1) to 0x42 (Java SE 22)
    // apple universal binaries use those four bytes for the number of architectures
    if (mem.eql(u8, "\xCA\xFE\xBA\xBE", file.buf[0..4])) {
        if (0x04 + 4 < file.size) {
            const fat_numarch_java_version = mem.readInt(u32, file.buf[4..][0..4], .big);
            // TODO are there fat binaries that support 0 architectures?
            if (fat_numarch_java_version > 0 and fat_numarch_java_version < 0x2D) {
                return true;
            }
        }
    }
    return false;
}

fn bit_end_universal(file: *utils.binFile) ?[]const u8 {
    switch (mem.readInt(u32, file.buf[0..4], .big)) {
        0xCAFEBABE => return "32-bit, big-endian",
        0xBEBAFECA => return "32-bit, little-endian",
        0xCAFEBABF => return "64-bit, big-endian",
        0xBFBAFECA => return "64-bit, little-endian",
        else => return null,
    }
    return null;
}

fn get_arches_universal(al: std.mem.Allocator, file: *utils.binFile) !?[]const []const u8 {
    if (0x04 + 4 < file.size) {
        const numarch = mem.readInt(u32, file.buf[4..][0..4], .big);
        const entries = file.buf[8..][0 .. numarch * 20];
        var archs: [][]const u8 = try al.alloc([]const u8, numarch);
        errdefer for (archs) |a| al.free(a);

        for (0..numarch) |i| {
            const entry = entries[i * 20 ..][0..20];
            const cpu_type = mem.readInt(u32, entry[0..4], .big) & ~@as(u32, 0x01000000);
            const cpu_subtype = mem.readInt(u32, entry[4..8], .big);
            switch (cpu_type) {
                0x00000007 => archs[i] = "x86",
                0x0000000C => archs[i] = "arm",
                0x00000012 => archs[i] = "ppc",
                else => return null,
            }
            if (cpu_type == 0x00000012) {
                switch (cpu_subtype) {
                    0x00000009 => archs[i] = "ppc 750",
                    0x0000000A => archs[i] = "ppc 7400",
                    0x00000064 => archs[i] = "ppc 970",
                    else => return null,
                }
            }
        }
        return archs;
    }
    return null;
}
