const std = @import("std");
const utils = @import("../utils.zig");
const identify = @import("../identify.zig");
const mem = std.mem;

// ELF (Executable and Linkable Format)
// https://en.wikipedia.org/wiki/Executable_and_Linkable_Format

pub const file_format = identify.FileFormat{
    .id = .elf,
    .name = "ELF",
    .magic_words = &.{
        "\x7FELF",
    },
    .get_bits_endian = bit_end,
    .get_arches = get_arches,
};

fn bit_end(file: *utils.binFile) ?[]const u8 {
    const ei_class_and_data = mem.readInt(u16, file.buf[4..][0..2], .big);
    switch (ei_class_and_data) {
        0x0101 => return "32-bit, little-endian",
        0x0201 => return "64-bit, little-endian",
        0x0102 => return "32-bit, big-endian",
        0x0202 => return "64-bit, big-endian",
        else => return null,
    }
}

fn get_arches(al: std.mem.Allocator, file: *utils.binFile) !?[]const []const u8 {
    const endian = switch (file.buf[5]) {
        1 => std.builtin.Endian.little,
        2 => std.builtin.Endian.big,
        else => return null,
    };
    // const os_abi = file.buf[7];
    // std.debug.print("  >> ELF e_ident[EI_OSABI]: 0x{x}\n", .{os_abi});
    // The PowerPC binary I have has 0x00, System V in the e_ident[EI_OSABI] field
    const arch = switch (mem.readInt(u16, file.buf[0x12..][0..2], endian)) {
        0x0003 => "x86",
        0x0014 => "ppc", // 32-bit
        0x0028 => "arm",
        0x003e => "x86_64", // aka x64, amd64
        0x00b7 => "aarch64", // aka armv8, arm64
        else => return null,
    };
    return try al.dupe([]const u8, &[_][]const u8{arch});
}
