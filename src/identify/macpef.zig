const std = @import("std");
const utils = @import("../utils.zig");
const identify = @import("../identify.zig");
const mem = std.mem;

// PEF (Preferred Executable Format)
// Also known as CFM (Code Fragment Manager)
// https://en.wikipedia.org/wiki/Preferred_Executable_Format
// https://web.archive.org/web/20020208214155/http://developer.apple.com/techpubs/mac/runtimehtml/RTArch-89.html
// Introduced with the PowerPC CPU until replaced by Mach-O

pub const file_format = identify.FileFormat{
    .id = .pef,
    .name = "Preferred Executable Format",
    .magic_words = &.{
        "Joy!peff",
    },
    .get_bits_endian = struct {
        fn get_bits_endian(file: *utils.binFile) ?[]const u8 {
            _ = file;
            return "32-bit, big-endian";
        }
    }.get_bits_endian,
    .get_arches = struct {
        fn get_arches(al: std.mem.Allocator, file: *utils.binFile) !?[]const []const u8 {
            const arch = if (mem.eql(u8, file.buf[8..12], "pwpc"))
                "ppc"
            else if (mem.eql(u8, file.buf[8..12], "m68k"))
                "m68k"
            else
                return null;

            return try al.dupe([]const u8, &[_][]const u8{arch});
        }
    }.get_arches,
};
