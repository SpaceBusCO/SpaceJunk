const std = @import("std");
const utils = @import("../utils.zig");
const identify = @import("../identify.zig");
const mem = std.mem;

pub const file_format = identify.FileFormat{
    .id = .ql,
    .name = "Sinclair QL",
    .verify = verify,
    .get_bits_endian = struct {
        fn get_bits_endian(file: *utils.binFile) ?[]const u8 {
            _ = file;
            return "32-bit, little-endian";
        }
    }.get_bits_endian,
    .get_arches = struct {
        fn get_arches(al: std.mem.Allocator, file: *utils.binFile) !?[]const []const u8 {
            _ = file;
            return try al.dupe([]const u8, &[_][]const u8{"m68k"});
        }
    }.get_arches,
};

fn verify(file: *utils.binFile) bool {
    // full header is 64 bytes
    if (file.size < 64) return false;
    const sig = mem.readInt(u16, file.buf[6..][0..2], .big);
    if (sig != 0x4AFB) return false;
    // filename field is apparently optional and doesn't prevent files being loaded and ran
    //   but QL filenames are max 36 chars long
    // if we still get false positives we can check that each char is 7-bit ASCII [0x20-0x80)
    const filename_len = mem.readInt(u16, file.buf[8..][0..2], .big);
    return filename_len <= 36;
}
