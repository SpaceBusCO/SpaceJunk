const std = @import("std");
const utils = @import("../utils.zig");
const identify = @import("../identify.zig");

// Atari ST executables
// http://justsolve.archiveteam.org/wiki/Atari_ST_executable

pub const file_format = identify.FileFormat{
    .id = .atari_st,
    .name = "Atari ST executable",
    .magic_words = &.{
        "\x60\x1a", // contiguous executable
        "\x60\x1b", // non-contiguous executable
    },
    .get_bits_endian = struct {
        fn get_bits_endian(file: *utils.binFile) ?[]const u8 {
            _ = file;
            return "32-bit, big-endian";
        }
    }.get_bits_endian,
    .get_arches = struct {
        fn get_arches(al: std.mem.Allocator, file: *utils.binFile) !?[]const []const u8 {
            _ = file;
            return try al.dupe([]const u8, &[_][]const u8{"m68k"});
        }
    }.get_arches,
};
