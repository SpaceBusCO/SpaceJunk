const std = @import("std");
const dump = @import("dump.zig");
const utils = @import("utils.zig");
const debug = std.debug;
const fs = std.fs;
const io = std.io;
const mem = std.mem;
const os = std.os;
const posix = std.posix;

pub fn shell(file: *utils.binFile) !void {
    _ = file;
    // This is based on the "unkooked" tutorial by Leon Henrik Plickat
    // https://zig.news/lhp/want-to-create-a-tui-application-the-basics-of-uncooked-terminal-io-17gm
    /////////////////////////////
    // Initialization process. //
    /////////////////////////////
    var tty = try fs.cwd().openFile("/dev/tty", .{ .mode = .read_write });
    defer tty.close();
    const original = try posix.tcgetattr(tty.handle);
    var raw = original;
    const writer = tty.writer();
    raw.iflag.IGNBRK = false;
    raw.iflag.BRKINT = false;
    raw.iflag.PARMRK = false;
    raw.iflag.INPCK = false;
    raw.iflag.ISTRIP = false;
    raw.iflag.INLCR = false;
    raw.iflag.IGNCR = false;
    raw.iflag.ICRNL = false;
    raw.iflag.IXON = false;
    raw.lflag.ICANON = false;
    raw.lflag.ECHO = false;
    raw.lflag.ECHONL = false;
    raw.lflag.IEXTEN = false;
    raw.lflag.ISIG = false;
    raw.cc[@intFromEnum(std.posix.system.V.TIME)] = 1;
    raw.cc[@intFromEnum(std.posix.system.V.MIN)] = 0;
    try posix.tcsetattr(tty.handle, .NOW, raw);
    //try writer.writeAll("\x1B[?25l"); // Hide the cursor.
    try writer.writeAll("\x1B[s"); // Save cursor position.
    try writer.writeAll("\x1B[?47h"); // Save screen.
    try writer.writeAll("\x1B[?1049h"); // Enable alternative buffer.
    ///////////////////
    // Init RE stuff //
    ///////////////////
    debug.print("> ", .{});
    var cmd = utils.doubleList{
        .values = std.ArrayList(u8).init(std.heap.page_allocator),
        .index = std.ArrayList(usize).init(std.heap.page_allocator),
    };
    while (true) {
        var buffer: [1]u8 = undefined;
        _ = try tty.read(&buffer);
        if (buffer[0] == '\x04') {
            try posix.tcsetattr(tty.handle, .FLUSH, original);
            return;
        } else if (buffer[0] == '\x05') {
            try posix.tcsetattr(tty.handle, .FLUSH, raw);
            debug.print("> ", .{});
        } else if (buffer[0] == ' ') {
            debug.print(" ", .{});
            try cmd.index.append(cmd.values.items.len);
        } else if (buffer[0] == '\n' or buffer[0] == '\r') {
            try cmd.index.append(cmd.values.items.len);
            try parse(&cmd);
            cmd.values.clearAndFree();
            cmd.index.clearAndFree();
            debug.print("\n> ", .{});
        } else if (buffer[0] == 170) {
            debug.print("", .{});
        } else {
            try cmd.values.append(buffer[0]);
            debug.print("{s}", .{buffer});
        }
    }
}

fn parse(cmd: *utils.doubleList) !void {
    if (mem.eql(u8, try cmd.get(0), "ls")) {
        var dir = try std.fs.cwd().openDir(".", .{ .iterate = true });
        defer dir.close();
        var iter = dir.iterate();
        debug.print("\n", .{});
        while (try iter.next()) |entry| {
            debug.print("{s} \n", .{entry.name});
        }
    }
    if (mem.eql(u8, try cmd.get(0), "hexdump")) {
        debug.print(" Not implemented yet. \n", .{});
        //try dump.hexdump(buf, false);
    }
}
