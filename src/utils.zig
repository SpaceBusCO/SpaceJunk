const std = @import("std");

// A basic implemtation of a list of arbitrarally sized strings.
// This functions by having an ArryList of characters, and an ArrayList of
// different sizes. This works because the slice of the desired string will
// exist in the slice of the index ArrayList. In other words, you can get
// index 3 of this list by slicing the ArrayList between the values in the
// index at indices 2 and 3.

var gpa = std.heap.GeneralPurposeAllocator(.{}){};
const GPallocator = gpa.allocator();

pub const doubleList = struct {
    values: std.ArrayList(u8),
    index: std.ArrayList(usize),

    //Initialize the list.
    pub fn init() doubleList {
        return doubleList{
            .values = std.ArrayList(u8).init(GPallocator),
            .index = std.ArrayList(usize).init(GPallocator),
        };
    }

    // Returns the specified index of the list.
    pub fn get(self: *doubleList, idx: u8) ![]const u8 {
        // To save memory, I did not include 0 as an index.
        if (idx == 0) {
            const val = self.values.items[0..self.index.items[idx]];
            return val;
        }
        const val = self.values.items[self.index.items[idx - 1]..self.index.items[idx]];
        return val;
    }
    // Appends the specified value to the list.
    pub fn append(self: *doubleList, item: []const u8) !void {
        try self.values.appendSlice(item);
        try self.index.append(self.values.items.len);
    }
};

pub const binFile = struct {
    buf: []u8,
    idx: u16,
    lang: []const u8,
    size: u64,
    strings: std.ArrayList([]const usize),
    entropy: std.ArrayList(f16),
    pub fn init(path: []const u8) !binFile {
        const stdout = std.io.getStdOut().writer();

        var file = std.fs.cwd().openFile(path, .{}) catch {
            try stdout.print("ERROR, cannot find file {s}\n", .{path});
            return error.FileNotfound;
        };
        defer {
            //file.close();
        }
        const file_size = (try file.stat()).size;

        const fileOut = binFile{
            .buf = try GPallocator.alloc(u8, file_size),
            .idx = 0,
            .lang = "Unknown",
            .size = file_size,
            .strings = std.ArrayList([]const usize).init(GPallocator),
            .entropy = std.ArrayList(f16).init(GPallocator),
        };

        try file.reader().readNoEof(fileOut.buf);
        file.close();
        return fileOut;
    }
};
