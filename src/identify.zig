const std = @import("std");
const utils = @import("utils.zig");
const microsoft = @import("identify/microsoft.zig");
const elf = @import("identify/elf.zig");
const macmacho = @import("identify/macmacho.zig");
const macrsrc = @import("identify/macrsrc.zig");
const macpef = @import("identify/macpef.zig");
const speccy = @import("identify/speccy.zig");
const amiga = @import("identify/amiga.zig");
const atarist = @import("identify/atarist.zig");
const ql = @import("identify/ql.zig");
const x68k = @import("identify/x68k.zig");
const riscos = @import("identify/riscos.zig");
const mem = std.mem;

// identify binary executable file formats

// an enum of the basic binary executable file formats
const FileFormatId = enum {
    ms_dos,
    windows_pe,
    elf,
    macho,
    macho_universal,
    mac_resource,
    pef,
    zx_sna,
    amiga_hunk,
    atari_st,
    ql,
    x68k_x,
    x68k_z,
    riscos,
};

pub const EntryPoint = struct {
    address: usize,
    file_offset: ?usize,
};

pub const FileFormat = struct {
    id: FileFormatId,
    name: []const u8,
    magic_words: ?[]const []const u8 = null,
    verify: ?*const fn (file: *utils.binFile) bool = null,
    get_bits_endian: ?*const fn (file: *utils.binFile) ?[]const u8 = null,
    get_arches: ?*const fn (al: std.mem.Allocator, file: *utils.binFile) anyerror!?[]const []const u8,
    get_code_blocks: ?*const fn (al: std.mem.Allocator, file: *utils.binFile) anyerror!?[]const []const u8 = null,
    get_entry_point: ?*const fn (file: *utils.binFile) ?EntryPoint = null,
};

const file_formats = [_]FileFormat{
    // Windows must come before MS-DOS
    microsoft.file_format_pe,
    // MS-DOS must come after Windows
    microsoft.file_format_mz,
    elf.file_format,
    macmacho.file_format,
    macmacho.file_format_universal,
    macrsrc.file_format,
    macpef.file_format,
    speccy.file_format,
    amiga.file_format,
    atarist.file_format,
    ql.file_format,
    x68k.file_format_x,
    x68k.file_format_z,
    riscos.file_format,
};

pub fn identify(al: std.mem.Allocator, file: *utils.binFile, path: []const u8) !void {
    const stdout = std.io.getStdOut().writer();

    const format: ?FileFormat =
        l: for (file_formats) |file_format|
    {
        // if there are magic words, one must match
        // then if there is a verify function, it must also pass
        if (file_format.magic_words) |magic_words| {
            for (magic_words) |magic| {
                if (magic.len < file.size and mem.eql(u8, magic, file.buf[0..magic.len]) and (file_format.verify == null or file_format.verify.?(file))) {
                    break :l file_format;
                }
            }
        } else {
            // if there are no magic words, there must be a verify function, and it must pass
            if (file_format.verify) |verify| if (verify(file)) break :l file_format;
        }
    } else null;

    if (format) |fmt| {
        try stdout.print("File: {s}\n", .{path});
        try stdout.print(" {s}\n", .{fmt.name});

        // TODO file type/version? PE32 vs PE32+; Mach-O vs Mach-O Universal

        // bitness? 16 vs 32 vs 64 - but is this about the cpu or the sizes of fields in the file?
        // endianness? little vs big - but is this about the cpu or the endianness of fields in the file?
        if (fmt.get_bits_endian) |get_bits_endian| if (get_bits_endian(file)) |bits_and_endianness| {
            try stdout.print("  Bits and endian: {s}\n", .{bits_and_endianness});
        };

        if (fmt.get_arches) |get_arches| if (try get_arches(al, file)) |arches| {
            defer al.free(arches);

            if (arches.len == 1) {
                try stdout.print("  Architecture: {s}\n", .{arches[0]});
            } else {
                for (arches, 1..) |arch, i| try stdout.print("  Architecture {d}: {s}\n", .{ i, arch });
            }
        };

        if (fmt.get_code_blocks) |get_code_blocks| if (try get_code_blocks(al, file)) |code_blocks| {
            defer al.free(code_blocks);

            for (code_blocks, 1..) |block, i| {
                try stdout.print("  Code block {}: 0x{x:0>4}, length 0x{x:0>4} ({})\n", .{
                    i, block.ptr - file.buf.ptr, block.len, block.len,
                });
            }
        };

        if (fmt.get_entry_point) |get_entry_point| if (get_entry_point(file)) |entry_point| {
            try stdout.print("  Entry point address: 0x{x:0>4}\n", .{entry_point.address});
            if (entry_point.file_offset) |offset| try stdout.print("  Entry point offset in file: 0x{x:0>5}\n", .{offset});
        };
    } else {
        try stdout.print("Not a binary/executable file\n", .{});
    }
}
