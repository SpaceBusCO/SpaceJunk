const std = @import("std");
const diss = @import("dissassemble.zig");
const dump = @import("dump.zig");
const shell = @import("shell.zig");
const utils = @import("utils.zig");
const analyze = @import("analyze.zig");
const identify = @import("identify.zig");
////////////////////////////////////////////

const debug = std.debug;

///////////////////////////////////////////

const version = "0.0.1-alpha3";
var verbose = false;

///////////////////////////////////////////

const help =
    \\ shell                      Open an interactive shell environment.
    \\ help                       Display this message and exit.
    \\ strings                    Find strings within file.
    \\ hexdump                    Displays hexdump of file. 
    \\ identify                   Identify file type.
;

pub fn main() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();
    const al = gpa.allocator();

    const args_alloc = try std.process.argsAlloc(al);
    defer std.process.argsFree(al, args_alloc);

    var args = utils.doubleList.init();
    for (args_alloc) |arg| {
        try args.append(arg);
    }
    var file = utils.binFile.init(try args.get(@intCast(args.index.items.len - 1))) catch |err| {
        debug.print("{s} \n\n", .{help});
        return err;
    };
    if (std.mem.eql(u8, "shell", try args.get(1))) {
        try shell.shell(&file);
    } else if (std.mem.eql(u8, "help", try args.get(1))) {
        debug.print("{s}\n", .{help});
    } else if (std.mem.eql(u8, "strings", try args.get(1))) {
        try analyze.strings(&file, true, 0.6);
        // TODO: that's awful formating. Change.
        //debug.print("{any}", .{file.strings.values.items});
    } else if (std.mem.eql(u8, "hexdump", try args.get(1))) {
        try dump.hexdump(&file);
    } else if (std.mem.eql(u8, "identify", try args.get(1))) {
        try identify.identify(al, &file, try args.get(2));
    } else {
        debug.print("{s} \n\n", .{help});
    }
}
