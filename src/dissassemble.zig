const std = @import("std");
const clap = @import("clap");
const toHex = std.fmt.fmtSliceHexLower;
const stdout = std.io.getStdOut().writer();
const stderr = std.debug.print;

pub fn dissX86(buf: []const u8, verbose: bool) !void {
    for (buf, 0..) |val, index| {
        _ = index;
        switch (val) {
            55 => {
                try stdout.print("{s}", .{"AAA"});
            },
            213 => {
                try stdout.print("{s}", .{"AAD"});
            },
            212 => {
                try stdout.print("{s}", .{"AAM"});
            },
            63 => {
                try stdout.print("{s}", .{"AAS"});
            },
            16...21, 128...131 => {
                try stdout.print("ADC\n", .{});
            },
            0...5 => {
                try stdout.print("ADD\n", .{});
            },
            32...37 => {
                try stdout.print("AND\n", .{});
            },
            else => {
                if (verbose) {
                    try stdout.print("{s}\n", .{"INVALID"});
                }
            },
        }
    }
}
