# SpaceJunk

A full featured reverse engineering suite for the hacker who is afraid to leave the terminal. 

## Background: 

I have been reverse engineering many things for many years, but never found a toolchain I liked. [Ghidra](https://ghidra-sre.org/) has no dark mode, which is unacceptable. [ImHex](https://imhex.werwolv.net/) is really good at analyzing data files, but has no disassembly built in. [Cutter](https://cutter.re/) was always a clunky interface, and [Rizin](https://rizin.re/) is confusing and poorly documented. I had experimented with custom disassemblers in the past, but nothing ever complete enough to be useful. 

Then I was thinking about how useful it would be to have a terminal-based logic analyzer utility, and it dawned upon me that much of the codebase needed for reverse engineering logic would be the same as reverse engineering binary files. So I started writing a suite of tools capable of reverse engineering everything from signals to binary files to hardware protocols. I wanted to be able to work in a terminal interface like Rizin, but also have a full command structure for shell scripting.  


## Features/TODO: 

- [X] Hexdumping (A little buggy, but it's getting there)
- [X] Interactive shell (needs more work, but I am fairly happy with it thus far)
- [ ] CLI parsing (Doing all that manually, which is taking more time to implement than I anticipated). 
- [ ] X86 disassembly (I have a basic implementation, but it's not useful as is). 
- [ ] RISC-V disassembly
- [ ] ARM disassembly
- [ ] Motorola 680X0 disassembly
- [ ] Zilog Z80 disassembly
- [ ] MOS 6502 disassembly
- [X] MS-DOS MZ executable format (Can identify only so far)
- [X] Windows PE executable format (Can identify only so far)
- [X] Linux ELF executable format (Can identify only so far)
- [X] Apple Mach-O executable format (Can identify only so far)
- [X] Classic Mac PEF executable format (Can identify only so far)
- [X] Classic Mac APPL resource fork format (Can identify and find code blocks so far)
- [X] Amiga "Hunks" executable format (Can identify only so far)
- [X] Atari ST executable format (Can identify only so far)
- [X] Sinclair ZX Spectrum emulator .SNA tape snapshot format
- [X] Sinclair QL executable format (Can identify only so far)
- [X] Sharp X68000 executable formats (Can identify two types out of the three only so far)
- [X] RISC OS executable format (Can identify only so far)
- [ ] Commodore 64 tape/disk formats
- [ ] Binary file analysis 
- [ ] Java analysis
- [ ] String search
- [ ] Basic decompiling
- [ ] ImHex Pattern Language implementation
- [ ] Interface with [SigRok](https://sigrok.org/wiki/Main_Page) hardware
- [ ] Interface with SDR hardware
- [ ] Much, much more. 

## Building: 

This is built against Zig version 0.11.0, and does not include any external dependencies. Simply run `zig build` and you will have a working binary. 


## Contributing: 

This is very early days, and I have no clue what I am doing, if you have any questions, comments, or concerns, please reach out. If you wish to contribute code, please ensure the code compiles, and run `zig fmt src/*`. 


